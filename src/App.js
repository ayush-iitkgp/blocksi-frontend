import './App.css';
import Login from './components/Login';
import Contacts from './components/Contacts';
import Home from './components/Home';
import Register from './components/Register';
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom';
import PrivateRoute from  './utils/PrivateRoute';
import PublicRoute from './utils/PublicRoute';
import {getToken, getUser, removeUserSession, setUserSession} from './utils/Common'
import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [authLoading, setAuthLoading] = useState(true);
 
  useEffect(() => {
    const user = getUser();
    if (!user) {
      return;
    }
    const loginUrl = 'http://localhost:8080/login';
    axios.post(loginUrl, 
			{ username: user.username, password: user.password })
			.then(response => {
				setAuthLoading(false);
				setUserSession(response.data.token, response.data.user);
    }).catch(error => {
      removeUserSession();
      setAuthLoading(false);
    });
  }, []);
 
  if (authLoading && getToken()) {
    return <div className="content">Checking Authentication...</div>
  }
  return (
    <div className="App">
      <BrowserRouter>
        <div>
          <div className="header">
            <NavLink exact activeClassName="active" to="/">Home</NavLink>
            <NavLink activeClassName="active" to="/login">Login</NavLink><small>(Access without token only)</small>
            <NavLink activeClassName="active" to="/home">Contacts</NavLink><small>(Access with token only)</small>
          </div>
          <div className="content">
            <Switch>
              <PublicRoute exact path="/" component={Home} />
              <PublicRoute path="/login" component={Login} />
              <PublicRoute path="/register" component={Register} />
              <PrivateRoute path="/home" component={Contacts} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}
 
export default App;