import React from 'react';
import { getUser, removeUserSession } from '../utils/Common';
 
function Contacts(props) {
  const user = getUser();
  // handle click event of logout button
  const handleLogout = () => {
    removeUserSession(); 
    props.history.push('/');
  }
 
  return (
    <div>
      Welcome {user.username}<br /><br />
      <input type="button" onClick={handleLogout} value="Logout" />
    </div>
  );
}
 
export default Contacts;