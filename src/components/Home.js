import React from 'react';
 
function Home(props) {

  const handleRegister = () => {
    props.history.push('/register');
  };

  const handleLogin = () => {
    props.history.push('/login');
  };

  return (
    <div>
      Welcome to the Home Page!
      <div><p>Not a user, please register</p><input type="button" value="Register" onClick={handleRegister}/></div>
      <div><p>Already a user, please login</p><input type="button" value="Login" onClick={handleLogin}/></div>
    </div>
  );
}
 
export default Home;