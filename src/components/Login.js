import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession, useFormInput, validateInput } from '../utils/Common';

function Login(props) {
	const [loading, setLoading] = useState(false);
	const username = useFormInput('');
	const password = useFormInput('');
	const [error, setError] = useState(null);

	// handle button click of login form
	const handleLogin = () => {
		const validationMessage = validateInput(username, password);
		if (validationMessage) {
			setError(validationMessage);
			return;
		}
		setError(null);
		setLoading(true);
		const loginUrl = 'http://localhost:8080/login';
		axios.post(loginUrl, 
			{ username: username.value, password: password.value })
			.then(response => {
				setLoading(false);
				setUserSession(response.data.token, response.data.user);
				props.history.push('/home');
		}).catch(error => {
			setLoading(false);
			if (error && error.response && error.response.status === 401) {
				setError(error.response.data);
			}
			else {
				setError("Something went wrong. Please try again later.");
			}
		});
	}

	return (
		<div>
			Login<br /><br />
			<div>
				Username<br />
				<input type="text" {...username} autoComplete="new-password" />
			</div>
			<div style={{ marginTop: 10 }}>
				Password<br />
				<input type="password" {...password} autoComplete="new-password" />
			</div>
			{error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
			<input type="button" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br />
		</div>
	);
}

export default Login;