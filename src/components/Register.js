import React, { useState } from 'react';
import axios from 'axios';
import { useFormInput, validateInput } from '../utils/Common';

function Register(props) {
	const username = useFormInput('');
	const password = useFormInput('');
	const [error, setError] = useState(null);

	// handle button click of register form
	const handleRegister = () => {
		const validationMessage = validateInput(username, password);
		if (validationMessage) {
			setError(validationMessage);
			return;
		}
		const registerUrl = 'http://localhost:8080/register';
		axios.post(registerUrl, 
			{ username: username.value, password: password.value })
			.then(response => {
				props.history.push('/login');
		}).catch(error => {
			if (error && error.response) {
				setError(error.response.data);
			}
		});
	}

	return (
		<div>
			Please Register<br /><br />
			<div>
				Username<br />
				<input type="text" {...username} autoComplete="new-password" />
			</div>
			<div style={{ marginTop: 10 }}>
				Password<br />
				<input type="password" {...password} autoComplete="new-password" />
			</div>
			{error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
			<input type="button" value={'Register'} onClick={handleRegister}/><br />
		</div>
	);
}


export default Register;