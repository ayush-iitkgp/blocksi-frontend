import { useState } from 'react';

// return the user data from the session storage
export const getUser = () => {
	const userStr = sessionStorage.getItem('user');
	if (userStr !== 'undefined') {
		return JSON.parse(userStr);
	}
	else {
		return undefined;
	}
}

// return the token from the session storage
export const getToken = () => {
	const token = sessionStorage.getItem('token');
	if (token !== 'undefined') {
		return token;
	}
	else {
		return undefined;
	}
}

// remove the token and user from the session storage
export const removeUserSession = () => {
	sessionStorage.removeItem('token');
	sessionStorage.removeItem('user');
}

// set the token and user from the session storage
export const setUserSession = (token, user) => {
	sessionStorage.setItem('token', token);
	sessionStorage.setItem('user', JSON.stringify(user));
}

export const useFormInput = initialValue => {
	const [value, setValue] = useState(initialValue);

	const handleChange = e => {
		setValue(e.target.value);
	}
	return {
		value,
		onChange: handleChange
	}
}

export const validateInput = (username, password) => {
	if (username.value.length === 0) {
		return ("Please enter user name");
	}
	if (password.value.length === 0) {
		return ("Please enter password");
	}
	return undefined;
}